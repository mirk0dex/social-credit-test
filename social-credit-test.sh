#!/usr/bin/env sh
#
# Deps:
# paplay (I know, Pulse is bloat but neither aplay or play would work for me)

## INITIAL VARS ##################################################################################

score=20 # initial score
sp="/-\|" # spinner thingy for loading effect
readonly availLangs="english, italian" # languages available to be setted through the -l flag

##################################################################################################

## FLAGS #########################################################################################

case $1 in
    --lang|-l|language|--language) if [ -z "$2" ]; then
                                       echo "Please specify the language you wanna use."
                                       echo "Available langs are:"
                                       echo "$availLangs"
                                       paplay "./assets/sfx/siren.opus" &
                                       exit 100
                                   fi
                                   case $2 in
                                       english) :
                                                ;; # it's the default
                                       italian) readonly lang="italian";;
                                       chinese) echo "Not available yet, sorry great leader Xi Jinping :("
                                                paplay "./assets/sfx/siren.opus" &
                                                exit 100
                                                ;;
                                       amogus)  echo "your so SUSssy!!1! ඞ"
                                                exit
                                                ;;
                                       *)       echo "Not a valid language!"
                                                echo "Available langs are:"
                                                echo "$availLangs"
                                                paplay "./assets/sfx/siren.opus" &
                                                exit 100
                                   esac;
esac

##################################################################################################


initialStuffIta() {
    paplay "./assets/music/Jin - If You Feel My Love.opus" > /dev/null & # some nice music
    clear

    echo " "
    echo "Benvenuti al Test del Credito Sociale!"
    echo "我们欢迎社会信贷试验!"
    echo " "

    echo "Ti verranno poste svariate domande e otterrai un"
    echo "punteggio sociale diverso a seconda delle tue risposte."
    echo " "

    echo "Questo punteggio influenzerà la tua vita quotidiana:"
    echo "ti darà privilegi se positivo, o, se negativo, ti"
    echo "toglierà molteplici diritti."
    echo " "

    echo "Premi invio quando sei pronto per iniziare."
    echo "La gloriosa RPC è dalla tua parte, buona fortuna!"
    echo " "

    read -r answer

    echo "Il tuo punteggio iniziale è 20."
    echo " "
    echo " "
    echo " "
}


initialStuffEng() {
    paplay "./assets/music/Jin - If You Feel My Love.opus" > /dev/null & # some nice music
    clear

    echo " "
    echo "Welcome to the Social Credit Test!"
    echo "我们欢迎社会信贷试验!"
    echo " "

    echo "You'll be asked a few questions and you'll get"
    echo "social credit scores based on your answers."
    echo " "

    echo "This score will influence your life: it'll give you"
    echo "privileges if positive, or, if negative, take away"
    echo "many of your rights."
    echo " "

    echo "Press enter when you're ready to begin."
    echo "The glorious PRC stands by your side, good luck!"
    echo " "

    read -r answer

    echo "Your starting score is 20."
    echo " "
    echo " "
    echo " "
}

printScoreIta() {
    echo "Il punteggio corrente è $score."
    if [ "$score" -lt "10" ]; then
        randomNumber=$(shuf -i 1-5 -n 1)
        if [ "$randomNumber" = "1" ]; then   echo "Fai attenzione!"
                                             echo " "
        elif [ "$randomNumber" = "2" ]; then echo "Dovresti tenerlo d'occhio."
                                             echo " "
        elif [ "$randomNumber" = "3" ]; then echo "Non è una buona cosa, per niente proprio."
                                             echo " "
        elif [ "$randomNumber" = "4" ]; then echo "Sii cosciente del fatto che se non si passa il test"
                                             echo "si viene bannati dalla gloriosa RPC..."
                                             echo " "
        elif [ "$randomNumber" = "5" ]; then echo "Oh oh..."
                                             echo "无。"
                                             echo " "
        fi
        paplay "./assets/sfx/siren.opus"
        paplay "./assets/sfx/siren.opus"
        paplay "./assets/sfx/siren.opus"
        paplay "./assets/sfx/siren.opus"
    else
        echo "Bene."
        echo " "
    fi
}

printScore() {
    echo "Current score is $score."
    if [ "$score" -lt "10" ]; then
        randomNumber=$(shuf -i 1-5 -n 1)
        if [ "$randomNumber" = "1" ]; then   echo "Be careful!"
                                             echo " "
        elif [ "$randomNumber" = "2" ]; then echo "You should keep an eye on it."
                                             echo " "
        elif [ "$randomNumber" = "3" ]; then echo "That is NOT good, at all."
                                             echo " "
        elif [ "$randomNumber" = "4" ]; then echo "Be aware that if you don't pass the test you're gonna"
                                             echo "get expelled from the glorious PRC..."
                                             echo " "
        elif [ "$randomNumber" = "5" ]; then echo "Uh oh..."
                                             echo "无。"
                                             echo " "
        fi
        paplay "./assets/sfx/siren.opus"
        paplay "./assets/sfx/siren.opus"
        paplay "./assets/sfx/siren.opus"
        paplay "./assets/sfx/siren.opus"
    else
        echo "Good."
        echo " "
    fi
}

postStuffIta() {
    printf "Sto calcolando il punteggio  "

    i=0
    while [ $i -le 30 ]; do # create suspense and give the user anxiety by slowing the program down for no reason at all, lol
        sleep 0.1
        printf '\b%.1s' "$sp"
        sp=${sp#?}${sp%???} # fake loading spinner thing
        i=$((i + 1))
    done
    printf "\n\n"

    unset retry
    printScoreIta

    printf "Caricamento...  "
    i=0
    while [ $i -le 10 ]; do
        sleep 0.1
        printf '\b%.1s' "$sp"
        sp=${sp#?}${sp%???}
        i=$((i + 1))
    done
    printf "\n\n\n"
}

postStuffEng() {
    printf "Calculating score  "

    i=0
    while [ $i -le 30 ]; do # create suspense and give the user anxiety by slowing the program down for no reason at all, lol
        sleep 0.1
        printf '\b%.1s' "$sp"
        sp=${sp#?}${sp%???}
        i=$((i + 1))
    done
    printf "\n\n"

    unset retry
    printScore

    printf "Loading...  "
    i=0
    while [ $i -le 10 ]; do
        sleep 0.1
        printf '\b%.1s' "$sp"
        sp=${sp#?}${sp%???}
        i=$((i + 1))
    done
    printf "\n\n\n"
}

question1Ita() {
    echo "Domanda 1: Quante ore ti è permesso giocare ai videogiochi ogni giorno?"
    echo "a. 1; b. 4; c. 9; d. 18"
    echo " "
    read -r answer
    echo " "
    case $answer in
        a) clear
           echo "Corretto! La RPC è fiera di te!"
           echo "Hai ottenuto 10 punti! Prossima domanda..."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 10));;
        b|c|d) if [ "$retry" = "1" ]; then
                   clear
                   echo "Oh oh... hai deluso la sacra RPC!"
                   echo "-10 crediti sociali :(. Prossima domanda..."
                   echo " "
                   paplay "./assets/sfx/fail.wav" &
                   score=$((score - 10))
               else
                   echo "Risposta errata! Un altro tentativo..."
                   echo " "
                   retry=1
                   question1Ita
               fi;;
        *) echo "Per favore rispondi con: 'a', 'b', 'c' or 'd'."
           echo " "
           question1Ita
    esac
}

question1Eng() {
    echo "Question 1: How many hours are you allowed to play videogames for every day?"
    echo "a. 1; b. 4; c. 9; d. 18"
    echo " "
    read -r answer
    echo " "
    case $answer in
        a) clear
           echo "Correct! The PRC is proud of you!"
           echo "You gained 10 points! Next question..."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 10));;
        b|c|d) if [ "$retry" = "1" ]; then
                   clear
                   echo "Oh oh... you disappointed the holy PRC!"
                   echo "-10 social credit :(. Next question..."
                   echo " "
                   paplay "./assets/sfx/fail.wav" &
                   score=$((score - 10))
               else
                   echo "Wrong answer! One more try..."
                   echo " "
                   retry=1
                   question1Eng
               fi;;
        *) echo "Please answer with: 'a', 'b', 'c' or 'd'."
           echo " "
           question1Eng
    esac
}

question2Ita() {
    echo "Domanda 2: Quale di questi posti ESISTE di sicuro?"
    echo "a. Taiwan; b. Hong Kong; c. Tibet; d. La potente RPC"
    echo " "
    read -r answer
    echo " "
    case $answer in
        d) clear
           echo "Esattamente! Continua così."
           echo "Hai ottenuto 30 punti in più! Andando avanti..."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 30));;
        a|b|c) if [ "$retry" = "1" ]; then
                   clear
                   echo "Oh oh... sbagliato!!"
                   echo "-30000 crediti sociali >:(. Prossima domanda..."
                   echo " "
                   paplay "./assets/sfx/fail.wav" &
                   score=$((score - 30000))
               else
                   echo "Risposta sbagliata! Riprova ancora una volta."
                   echo " "
                   retry=1
                   question2Ita
               fi;;
        *) echo "Per favore rispondi con: 'a', 'b', 'c' or 'd'."
           echo " "
           question2Ita
    esac
}

question2Eng() {
    echo "Question 2: Which one of these countries DOES indeed exist?"
    echo "a. Taiwan; b. Hong Kong; c. Tibet; d. The powerful PRC"
    echo " "
    read -r answer
    echo " "
    case $answer in
        d) clear
           echo "Exactly! Keep it up."
           echo "You obtained more 30 points! Moving on..."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 30));;
        a|b|c) if [ "$retry" = "1" ]; then
                   clear
                   echo "Uh oh... wrong!!"
                   echo "-30000 social credit >:(. Next..."
                   echo " "
                   paplay "./assets/sfx/fail.wav" &
                   score=$((score - 30000))
               else
                   echo "Wrong answer! Retry one more time."
                   echo " "
                   retry=1
                   question2Eng
               fi;;
        *) echo "Please answer with: 'a', 'b', 'c' or 'd'."
           echo " "
           question2Eng
    esac
}

question3Ita() {
    echo "Domanda 3: Ti piace Winnie the Pooh?"
    echo "y. Sì, è così carino e morbidoso; n. No! Non mi piace per niente; ?. Cosa caspita è Winnie the Pooh? Mai sentito."
    echo " "
    read -r answer
    echo " "
    case $answer in
        [n/N]) clear
               echo "Sì, è la risposta giusta!!! Ben fatto ;)."
               echo "Hai ottenuto 20 punti in più! Prossima domanda..."
               echo " "
               paplay "./assets/sfx/mission-accomplished.wav" &
               score=$((score + 30));;
        [y/Y]) if [ "$retry" = "1" ]; then
                   clear
                   echo "Sbagliato."
                   echo "-300 crediti sociali. Prossima domanda..."
                   echo " "
                   paplay "./assets/sfx/fail.wav" &
                   score=$((score - 300))
               else
                   echo "È INCORRETTO! Ultima possibilità."
                   echo " "
                   retry=1
                   question3Ita
               fi;;
        ?) clear
           echo "Wow, te la cavi egregiamente."
           echo "O almeno, abbastanza da meritarti +40000 crediti!"
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 40000));;
        *) echo "Per favore rispondi con: 'y', 'n' or '?'."
           echo " "
           question3Ita
    esac
}

question3Eng() {
    echo "Question 3: Do you like Winnie the Pooh?"
    echo "y. Yes, he is so cute and fluffy; n. No! I don't, at all; ?. What the hecc is Winnie the Pooh? Never heard of it."
    echo " "
    read -r answer
    echo " "
    case $answer in
        [n/N]) clear
               echo "Yes, that's the right answer!!! Well done ;)."
               echo "You obtained 20 more points! Next..."
               echo " "
               paplay "./assets/sfx/mission-accomplished.wav" &
               score=$((score + 30));;
        [y/Y]) if [ "$retry" = "1" ]; then
                   clear
                   echo "Wrong."
                   echo "-300 social credit. Next queston..."
                   echo " "
                   paplay "./assets/sfx/fail.wav" &
                   score=$((score - 300))
               else
                   echo "That is INCORRECT! Last chance."
                   echo " "
                   retry=1
                   question3Eng
               fi;;
        ?) echo "Wow, you're so good at this."
           echo "Enough to deserve +40000 credit score!"
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 40000));;
        *) echo "Please answer with: 'y', 'n' or '?'."
           echo " "
           question3Eng
    esac
}

question4Ita() {
    echo "Domanda 4: Qual è la salsa migliore al mondo?"
    echo "a. Lao Gan Ma"
    echo " "
    read -r answer
    clear
    echo "Grazie al nostro sponsor: Lao Gan Ma! Hanno deciso di donarti 15 punti bonus!"
    echo "E ricorda: usa il codice 'EMACS69' per il 60% di sconto sul tuo primo acquisto!"
    echo "Lao Gan Ma! LAO GAN MA!"
    echo " "
    paplay "./assets/sfx/mission-accomplished.wav" &
    score=$((score + 15))
}

question4Eng() {
    echo "Question 4: Which is the best sauce in the world?"
    echo "a. Lao Gan Ma"
    echo " "
    read -r answer
    clear
    echo "Thanks to our sponsor: Lao Gan Ma! They decided to donate 15 bonus points to you!"
    echo "And remember: use code 'EMACS69' for 60% off your first order! Lao Gan Ma! LAO GAN MA!"
    echo " "
    paplay "./assets/sfx/mission-accomplished.wav" &
    score=$((score + 15))
}

question5Ita() {
    echo "Domanda 5: Quanto fa 9 + 10?"
    echo "a. 21; b. Государственный; c. 69420"
    echo " "
    read -r answer
    echo " "
    case $answer in
        a) clear
           echo "Bene! Qui con noi c'è niente meno che una persona acculturata."
           echo "Ti meriti 42096 punti come ricompensa..."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 42096));;
        b|c) if [ "$retry" = "1" ]; then
                clear
                echo "UUUH che cringe!!"
                echo "-2000 crediti sociali. Andiamo avanti, vah..."
                echo " "
                paplay "./assets/sfx/fail.wav" &
                score=$((score - 2000))
           else
                echo "NON CORRETTO! Un'altra possibilità."
                echo " "
                retry=1
                question5Ita
           fi;;
        *) echo "Per favore rispondi con: 'a', 'b' and/or 'c'."
           echo " "
           question5Ita
    esac
}

question5Eng() {
    echo "Question 5: What is 9 + 10?"
    echo "a. 21; b. Государственный; c. 69420"
    echo " "
    read -r answer
    echo " "
    case $answer in
        a) clear
           echo "Great! We have nothing less than a person of culture here."
           echo "You deserve 42096 points as a reward..."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 42096));;
        b|c) if [ "$retry" = "1" ]; then
                clear
                echo "OOOH cringe!!"
                echo "-2000 social credit. Let's move forward..."
                echo " "
                paplay "./assets/sfx/fail.wav" &
                score=$((score - 2000))
           else
                echo "NOT CORRECT! One more chance."
                echo " "
                retry=1
                question5Eng
           fi;;
        *) echo "Please answer with: 'a', 'b' and/or 'c'."
           echo " "
           question5Eng
    esac
}

question6Ita() {
    paplay "./assets/sfx/FNAF-2-hallway.opus" &
    echo "ULTIMA DOMANDA! Domanda 6: Chi è il miglior capo?"
    echo "a. The Wok;    b. Il grande Xi Jinping;"
    echo "c. Zhong Xina; d. Download Trampoli;"
    echo "e. Yo Mama"
    echo " "
    read -r answer
    echo " "
    case $answer in
        b) clear
           echo "SÌ! Sai il fatto tuo, persona avente cittadinanza."
           echo "+9734 points."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 9734));;
        a|c|d|e) if [ "$retry" = "1" ]; then
                clear
                echo "NOOO!! La sacra RPC si vergogna di te."
                echo "-un fantastilione di mila crediti..."
                echo " "
                paplay "./assets/sfx/fail.wav" &
                score=$((score - 1758517873818391718))
           else
                echo "No... ora sì, questa è veramente la tua ultima chance."
                echo " "
                retry=1
                question6Ita
           fi;;
        *) echo "Per favore rispondi con: 'a', 'b', 'c', 'd' or 'e'."
           echo " "
           question6Ita
    esac
}

question6Eng() {
    paplay "./assets/sfx/FNAF-2-hallway.opus" &
    echo "LAST QUESTION! Question 6: Who's the best leader?"
    echo "a. The Wok;    b. The great Xi Jinping;"
    echo "c. Zhong Xina; d. Download Trumpet;"
    echo "e. Joe Mama"
    echo " "
    read -r answer
    echo " "
    case $answer in
        b) clear
           echo "YES! You know whatcha talkin' about, citizen."
           echo "+9734 points."
           echo " "
           paplay "./assets/sfx/mission-accomplished.wav" &
           score=$((score + 9734));;
        a|c|d|e) if [ "$retry" = "1" ]; then
                clear
                echo "NOOO!! The holy PRC is ashamed of you."
                echo "-9q5885176974518000 credits..."
                echo " "
                paplay "./assets/sfx/fail.wav" &
                score=$((score - 1758517873818391718))
           else
                echo "Nope... now this really is your last chance."
                echo " "
                retry=1
                question6Eng
           fi;;
        *) echo "Please answer with: 'a', 'b', 'c', 'd' or 'e'."
           echo " "
           question6Eng
    esac
}

finalStuffIta() {
    kill "$(pidof paplay)" # stop music and sfx
    sleep 2
    if [ "$score" -ge 30 ]; then
        paplay "./assets/music/victory.opus" &
        printf "CONGRATULAZIONI! Hai un credito sociale di\n"
        printf "%d! " "$score"; if [ "$score" -ge 1500 ]; then printf "Non male, non male.\n"; fi
        printf "Puoi tornare a lavoro ora; ce l'hai un lavoro, no?\n\n"
        printf "[Game over. Premi C-c per uscire]\n"
        read -r answer
    else
        paplay "./assets/sfx/wrong-buzzer.wav"
        printf "Bene bene bene... indovina chi ha failato il test del credito sociale?\n"
        printf "Il tuo punteggio è di %d, sai cosa significa?\n" "$score"
        paplay "./assets/sfx/sudden-impact.wav"
        printf "La tua esecuzione pubbilca sarà domani.\n\n"
        paplay "./assets/sfx/heaven.opus" &
        printf "[Game over. Premi C-c per uscire]\n"
        read -r answer
    fi
}

finalStuffEng() {
    kill "$(pidof paplay)" # stop music and sfx
    sleep 2
    if [ "$score" -ge 30 ]; then
        paplay "./assets/music/victory.opus" &
        printf "CONGRATULATIONS! You have a social credit score of\n"
        printf "%d! " "$score"; if [ "$score" -ge 1500 ]; then printf "Woah!\n"; fi
        printf "You can get back to your workplace; you have one, right?\n\n"
        printf "[Game over. Press C-c to exit]"
        read -r answer
    else
        paplay "./assets/sfx/wrong-buzzer.wav"
        printf "Well well well... guess who's failed the social credit test?\n"
        printf "Your social credit score is %d, you know what that means?" "$score"
        paplay "./assets/sfx/sudden-impact.wav"
        printf "You will be publicly executed tomorrow.\n\n"
        paplay "./assets/sfx/heaven.opus" &
        printf "[Game over. Press C-c to exit]\n"
        read -r answer
    fi
}


# DO THE ACTUAL STUFF
if [ "$lang" = "italian" ] || [ "$LANG" = "it_IT.UTF8" ] || [ "$LANG" = "it_IT@euro" ]; then
    initialStuffIta
    question1Ita && postStuffIta
    question2Ita && postStuffIta
    question3Ita && postStuffIta
    question4Ita && postStuffIta
    question5Ita && postStuffIta
    question6Ita && postStuffIta && finalStuffIta
else
    initialStuffEng
    question1Eng && postStuffEng
    question2Eng && postStuffEng
    question3Eng && postStuffEng
    question4Eng && postStuffEng
    question5Eng && postStuffEng
    question6Eng && postStuffEng && finalStuffEng
fi

kill "$(pidof paplay)" # stop music and sfx
